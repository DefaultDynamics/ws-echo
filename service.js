
var Primus = require('primus')
  , https = require('https')
  , http = require('http')
  , fs = require('fs');

//var server = http.Server();

var server = https.createServer({
 requestCert: true,
 rejectUnauthorized: false,
 key: fs.readFileSync('/home/ec2-user/.ssl/vfa.io.key'),
 cert: fs.readFileSync('/home/ec2-user/.ssl/vfa.io.prod.crt'),
 ca: fs.readFileSync('/home/ec2-user/.ssl/vfa.io.prod.bundle.crt')
});

var port = process.env.PORT || 3500;

var headers = [
  'x-websocket-protocol', 
  'x-websocket-version', 
  'x-websocket-extensions', 
  'authorization', 
  'content-type'
];

var primus = new Primus(server, { credentials: true, headers: headers,  transformer: 'websockets', parser: 'JSON', exposed: true });

primus.on('connection', function (spark) {
  spark.on('data', function ondata(message) {
    spark.write(message);
  });
});

// Start server listening
server.listen(port, function(){
  console.log('\033[96mlistening on echo.vfa.io:' + port +' \033[39m');
});
