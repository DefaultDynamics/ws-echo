"use strict";

    var fs = require('fs');

    // you'll probably load configuration from config
    var cfg = {
        ssl: true,
        port: 3500,
        ssl_key: '/home/ec2-user/.ssl/vfa.io.key',
        ssl_cert: '/home/ec2-user/.ssl/vfa.io.prod.crt',
        ssl_ca: '/home/ec2-user/.ssl/vfa.io.prod.bundle.crt'
    };

    var httpServ = ( cfg.ssl ) ? require('https') : require('http');

    var WebSocketServer   = require('ws').Server;

    var app      = null;

    // dummy request processing
    var processRequest = function( req, res ) {

        res.writeHead(200);
        res.end("All glory to WebSockets!\n");
    };

    if ( cfg.ssl ) {

        app = httpServ.createServer({

            // providing server with  SSL key/cert
            key: fs.readFileSync( cfg.ssl_key ),
            cert: fs.readFileSync( cfg.ssl_cert ),
            ca: fs.readFileSync(cfg.ssl_ca)

        }, processRequest ).listen( cfg.port );

    } else {

        app = httpServ.createServer( processRequest ).listen( cfg.port );
    }

    // passing or reference to web server so WS would knew port and SSL capabilities
    var wss = new WebSocketServer( { server: app } );


    wss.on( 'connection', function ( socket ) {

        socket.on( 'message', function ( message ) {

            console.log( message );
            socket.send( message );

        });

    });



/*
var WebsocketServer = require('ws').Server;
var port = process.env.PORT || 3500;
var ws = new WebsocketServer({ port: port });

ws.on('connection', function (socket) {
 console.log('client connected ====>');
 socket.on('message', function (message) {
  socket.send(message);
 });
});


*/
